import pandas as pd
import json

dataset_url = './data/training-data/mbti-training-data.json'
with open(dataset_url, encoding="utf8") as f:
    data = json.loads(f.read())

def restructureDocument(dataset, train=True):

    restructured_Data = []

    print("")
    print("Restructuring Started...")

    for i in range(len(dataset)):
        document = dataset[i]
        if train:
            splitData = document["posts"].split('|||')
        else:
            splitData = document["posts"].split('.')

        restructured_Data.append({"type": document["type"], "posts": splitData})

    print("Restructuring Done...")
    return restructured_Data

# restructureDocument(data, train=True)

# with open('./data/clean-data/sample.json', 'w') as outfile:
#             json.dump(restructureDocument(data, train=True), outfile, indent=2)
