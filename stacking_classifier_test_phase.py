import re
import json
import nltk
import pickle
import gensim
import pprint
import logging
import asyncio
import warnings
import multiprocessing
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np
from time import time
from sklearn.preprocessing import scale
from nltk.corpus import stopwords
from matplotlib import pyplot
from gensim.models.callbacks import CallbackAny2Vec
from gensim.models import Word2Vec
from sklearn.pipeline import Pipeline
from sklearn.manifold import TSNE
from sklearn.feature_extraction.text import TfidfVectorizer
from nltk import word_tokenize
from nltk.stem import PorterStemmer, WordNetLemmatizer
from restructure import restructureDocument
from deduplication import deDuplication
from NER import recognizeEntity
from portfolio import createPortfolio
from smart_open import open

logging.basicConfig(filename='personality-analysis.log', level=logging.DEBUG)
warnings.filterwarnings("ignore", category=DeprecationWarning)

# plotting
plt.style.use('ggplot')

test_dataset_url = './data/test-data/test-data.json'
with open(test_dataset_url, encoding="utf") as f:
    test_dataset = json.loads(f.read())
    

# Converted Personality type lable to Binarized Format
binary_Personality = {'I': 0, 'E': 1, 'N': 0, 'S': 1, 'F': 0, 'T': 1, 'J': 0, 'P': 1}
binary_Personality_list = [
    {0: 'I', 1: 'E'}, 
    {0: 'N', 1: 'S'},
    {0: 'F', 1: 'T'}, 
    {0: 'J', 1: 'P'}]


def translate_personality(personality):
    # transform mbti label to binary vector
    return [binary_Personality[l] for l in personality]


def translate_back(personality):
    # transform binary vector to mbti personality label
    s = ""
    for i, l in enumerate(personality):
        s += binary_Personality_list[i][l]
    return s


# We want to remove these from the psosts
unique_type_list = ['INFJ', 'ENTP', 'INTP', 'INTJ', 'ENTJ', 'ENFJ', 'INFP', 'ENFP',
                    'ISFP', 'ISTP', 'ISFJ', 'ISTJ', 'ESTP', 'ESFP', 'ESTJ', 'ESFJ']

unique_type_list = [x.lower() for x in unique_type_list]


# Lemmatize
stemmer = PorterStemmer()
lemmatiser = WordNetLemmatizer()

# Cache the stop words for speed
cachedStopWords = stopwords.words("english")


def pre_process_data(data, train, restructure, data_deduplication, NER, remove_stop_words, remove_mbti_profiles):

    print("")
    print("Preprocessing Started...")

    pre_processed_dataset = []
    personality_label_list = []
    personality_posts_list = []
    # dataset_length = len(data)

    if restructure:
        pre_processed_dataset = restructureDocument(data, train)

    if data_deduplication and train==False:
        pre_processed_dataset = deDuplication(pre_processed_dataset)

    if NER:
        pre_processed_dataset = recognizeEntity(pre_processed_dataset)
        

    for row in range(len(pre_processed_dataset)):
        # Remove and clean comments
        posts = pre_processed_dataset[row]['posts']
        temp = " ".join(posts)
        temp = temp.strip()
        if remove_stop_words:
            temp = " ".join([lemmatiser.lemmatize(w)
                             for w in temp.split(' ') if w not in cachedStopWords])
        else:
            temp = " ".join([lemmatiser.lemmatize(w) for w in temp.split(' ')])

        if remove_mbti_profiles:
            for t in unique_type_list:
                temp = temp.replace(t, "")

        type_labelized = translate_personality(pre_processed_dataset[row]['type'])
        personality_label_list.append(type_labelized)
        personality_posts_list.append(temp)
        # personality_posts_list.append(" ".join(row[1].posts))

    personality_posts_list = np.array(personality_posts_list)
    personality_label_list = np.array(personality_label_list)

    print("")
    print("Preprocessing Done...")

    return personality_posts_list, personality_label_list

user_posts, dummy_label = pre_process_data(test_dataset, train=False, restructure=True, data_deduplication=True, NER=True, remove_stop_words=True, remove_mbti_profiles=False)

# print(len(personality_posts_list))
# print(user_posts[0])

print("")

print("Applying Tokenization...")
# TF-IDF + Word2Vec
def w2v_tokenize_text(text):
    tokens = []
    for sent in nltk.sent_tokenize(text, language='english'):
        for word in nltk.word_tokenize(sent, language='english'):
            if len(word) < 2:
                continue
            tokens.append(word)
    
    return tokens
print("Tokenization Done...")

user_posts = list(user_posts)

tokenized_user_post = [w2v_tokenize_text(i) for i in user_posts]

# WORD2VEC()
class EpochLogger(CallbackAny2Vec):
    '''Callback to log information about training'''

    def __init__(self):
        self.epoch = 0

    def on_epoch_begin(self, model):
        print("Epoch #{} start".format(self.epoch))

    def on_epoch_end(self, model):
        print("Epoch #{} end".format(self.epoch))
        self.epoch += 1

epoch_logger = EpochLogger()

# Word2Vec Model Training
# cores = multiprocessing.cpu_count() # Count the number of cores in a computer, important for a parameter of the model
# w2v_model = Word2Vec(min_count=20,
#                      window=2,
#                      size=1500,
#                      sample=6e-5,
#                      alpha=0.03,
#                      min_alpha=0.0007,
#                      negative=20,
#                      workers=cores-1,
#                      callbacks=[epoch_logger])

# #BUILD_VOCAB()
# t = time()
# w2v_model.build_vocab(tokenized_post, progress_per=1000)
# print('Time to build vocab: {} mins'.format(round((time() - t) / 60, 2)))

# #TRAIN()
# w2v_model.train(tokenized_post, total_examples=w2v_model.corpus_count, epochs=100, report_delay=1)
# print('Time to train the model: {} mins'.format(round((time() - t) / 60, 2)))

# w2v_model.save('w2v_model_1500')


# Word2Vec Trained Model
w2v_model = gensim.models.Word2Vec.load('word2vec_model/w2v_model_1500')

print("")

print("Applying TF-IDF Vectorization...")

vectorizer = TfidfVectorizer(analyzer="word",
                             max_features=1500,
                             tokenizer=None,
                             preprocessor=None,
                             stop_words=None,
                             lowercase=True)

#Testing Matrix
matrix = vectorizer.fit_transform([x for x in user_posts])

tfidf = dict(zip(vectorizer.get_feature_names(), vectorizer.idf_))
# print('vocab size: ', len(tfidf))


print("TF-IDF Vectorization Done...")

print("")

print("Building Word Vector: TF-IDF * Word2vec Model")
def buildWordVector(tokens, size):
    
    vec = np.zeros(size).reshape((1, size))
    count = 0.
    for word in tokens:
        try:
            vec += w2v_model[word].reshape((1, size)) * tfidf[word]
            count += 1.
        except KeyError:  # handling the case where the token is not
            # in the corpus. useful for testing.
            continue
    if count != 0:
        vec /= count

    return vec
print("Building Word Vector Done...")

#Testing Word Vector    
test_vecs_w2v = np.concatenate(
    [buildWordVector(z, 1500) for z in map(lambda x: x, tokenized_user_post)])
test_vecs_w2v = scale(test_vecs_w2v)
# print(test_vecs_w2v.shape)

type_indicators = ["IE: Introversion (I) - Extroversion (E)", 
                   "NS: Intuition (N) - Sensing (S)",
                   "FT: Feeling (F) - Thinking (T)", 
                   "JP: Judging (J) - Perceiving (P)"]

mbti_dimensions = ["I-E", "N-S", "F-T", "J-P"]

# Posts in  tf-idf + Word2Vec representation
USER_X = test_vecs_w2v

output = []

print("")
print("----------------------- User Personality Portfolio ----------------------------")
print("")
print("Personality Probabilities")
# Let's train type indicator individually -- XGBoost
for l in range(len(type_indicators)):
    # print("%s ..." % (type_indicators[l]))

    with open('models_test/PA_model_' + mbti_dimensions[l] + '.pickle', 'rb') as f:
        stacking_model = pickle.load(f)

    # make predictions for test data
    y_pred = stacking_model.predict(USER_X)
    predictions = [round(value) for value in y_pred]

    y_pred_prob = stacking_model.predict_proba(USER_X)

    dimension = type_indicators[l].split("-")
    
    print("%s : %.2f%% -- %s : %.2f%%" % (dimension[0], (y_pred_prob[0][0] * 100), dimension[1], (y_pred_prob[0][1] * 100)))

    output.append(y_pred[0])
    

# print("The output is: ", translate_back(output))

portfolio = createPortfolio(translate_back(output))

print("")
print("Personality Label: ", portfolio["label"])
print("Personality Title: ", portfolio["title"])
print("Personality Dimensions: ", portfolio["dimensions"])
print("Personality Characteristics: ", portfolio["characteristic"])
print("Personality Description: ", portfolio["description"])