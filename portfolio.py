import json

personality_info_url = 'personality.json'
with open(personality_info_url, encoding="utf8") as f:
    personality_info = json.loads(f.read())


def createPortfolio(label):

    portfolio_data = {}

    for i in range(len(personality_info)):
        personality_type = personality_info[i]
        if personality_type:
            if personality_type["label"] == label:
                portfolio_data = personality_type
    
    return portfolio_data

# print(createPortfolio("INFP"))