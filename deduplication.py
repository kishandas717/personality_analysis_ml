from simhash import Simhash
import pandas as pd
import json

def simhash_similarity(text1, text2):
    """
         :param tex1: text 1
         :param text2: text 2
         :return: returns the similarity of two articles
    """
    aa_simhash = Simhash(text1)
    bb_simhash = Simhash(text2)
    #
    distince = aa_simhash.distance(bb_simhash)
    # print("distance: ", distince)

    a = float(aa_simhash.value)
    b = float(bb_simhash.value)

    if a > b:
        similar = b / a
    else:
        similar = a / b

    return similar


# dataset_url = './data/training-data/spilted-mbti-training-data.json'
# with open(dataset_url, encoding="utf8") as f:
#         data = json.loads(f.read())


def deDuplication(dataset):

    deduplicated_data = []

    print("")
    print("Data Deduplication Started...")

    for i in range(len(dataset)):
        document = dataset[i]
        if document != None:
            posts = []
            for j in range(len(document['posts'])):
                x = document['posts'][j]
                similar = 0
                for k in range(len(document['posts'])-1):
                    y = document['posts'][k]
                    if y != None:
                        similar = simhash_similarity(x, y)
                if (similar > 0 and similar <= 0.9):
                    posts.append(x)
            # deduplicated_data.append({"type": document['type'], "BeforeLength": len(document["posts"]), "AfterLength": len(posts), "posts": posts})
            deduplicated_data.append({"type": document['type'], "posts": posts})
    
    print("Data Deduplication Done...")

    return deduplicated_data 

# deDuplication(data)

# with open('./data/clean-data/sample.json', 'w') as outfile:
#             json.dump(deDuplication(data), outfile, indent=2)