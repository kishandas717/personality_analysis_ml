Resample:Over-Sampling Dataset
Feature Engineering: TF-IDF
Classification: XGBoost
Parameter: param['n_estimators'] = 200
           param['max_depth'] = 2
           param['nthread'] = 8
           param['learning_rate'] = 0.1
           param['early_stopping_rounds'] = 10

IE: Introversion (I) - Extroversion (E) ...
Training Score:  86.69647847959754
Testing Score:  84.91036986612208
Testing.....
* IE: Introversion (I) - Extroversion (E) Accuracy: 84.91%
* IE: Introversion (I) - Extroversion (E) Precision: 93.37%
* IE: Introversion (I) - Extroversion (E) Recall: 74.84%
* IE: Introversion (I) - Extroversion (E) F1 Score: 83.08%
NS: Intuition (N) – Sensing (S) ...
Training Score:  89.9001996007984
Testing Score:  87.31766612641815
Testing.....
* NS: Intuition (N) – Sensing (S) Accuracy: 87.32%
* NS: Intuition (N) – Sensing (S) Precision: 95.24%
* NS: Intuition (N) – Sensing (S) Recall: 78.98%
* NS: Intuition (N) – Sensing (S) F1 Score: 86.35%
FT: Feeling (F) - Thinking (T) ...
Training Score:  83.66989982509143
Testing Score:  75.02420135527589
Testing.....
* FT: Feeling (F) - Thinking (T) Accuracy: 75.02%
* FT: Feeling (F) - Thinking (T) Precision: 73.93%
* FT: Feeling (F) - Thinking (T) Recall: 76.63%
* FT: Feeling (F) - Thinking (T) F1 Score: 75.26%
JP: Judging (J) – Perceiving (P) ...
Training Score:  78.3822272856736
Testing Score:  72.60115606936417
Testing.....
* JP: Judging (J) – Perceiving (P) Accuracy: 72.60%
* JP: Judging (J) – Perceiving (P) Precision: 68.74%
* JP: Judging (J) – Perceiving (P) Recall: 83.92%
* JP: Judging (J) – Perceiving (P) F1 Score: 75.58%



Resample:Over-Sampling Dataset
Feature Engineering: TF-IDF + Word2Vec
Classification: XGBoost
Parameter: param['n_estimators'] = 400
           param['max_depth'] = 6
           param['learning_rate'] = 0.1
Training:  80%
Testing:   20%

IE: Introversion (I) - Extroversion (E) ...
Before Smote:  Counter({0: 6676, 1: 1999})
After Smote:  Counter({0: 6676, 1: 6676})
Training Score:  100.0
Testing Score:  87.83227255709473
Testing.....
* IE: Introversion (I) - Extroversion (E) Accuracy: 87.83%
* IE: Introversion (I) - Extroversion (E) Precision: 86.61%
* IE: Introversion (I) - Extroversion (E) Recall: 89.17%
* IE: Introversion (I) - Extroversion (E) F1 Score: 87.87%
NS: Intuition (N) – Sensing (S) ...
Before Smote:  Counter({0: 7478, 1: 1197})
After Smote:  Counter({0: 7478, 1: 7478})
Training Score:  100.0
Testing Score:  95.78877005347593
Testing.....
* NS: Intuition (N) – Sensing (S) Accuracy: 95.79%
* NS: Intuition (N) – Sensing (S) Precision: 95.07%
* NS: Intuition (N) – Sensing (S) Recall: 96.91%
* NS: Intuition (N) – Sensing (S) F1 Score: 95.98%
FT: Feeling (F) - Thinking (T) ...
Before Smote:  Counter({0: 4694, 1: 3981})
After Smote:  Counter({0: 4694, 1: 4694})
Training Score:  100.0
Testing Score:  77.68903088391906
Testing.....
* FT: Feeling (F) - Thinking (T) Accuracy: 77.69%
* FT: Feeling (F) - Thinking (T) Precision: 75.45%
* FT: Feeling (F) - Thinking (T) Recall: 79.54%
* FT: Feeling (F) - Thinking (T) F1 Score: 77.44%
JP: Judging (J) – Perceiving (P) ...
Before Smote:  Counter({1: 5241, 0: 3434})
After Smote:  Counter({0: 5241, 1: 5241})
Training Score:  100.0
Testing Score:  71.95994277539342
Testing.....
* JP: Judging (J) – Perceiving (P) Accuracy: 71.96%
* JP: Judging (J) – Perceiving (P) Precision: 71.93%
* JP: Judging (J) – Perceiving (P) Recall: 73.28%
* JP: Judging (J) – Perceiving (P) F1 Score: 72.60%
            
Best Param:  {'learning_rate': 0.1, 'max_depth': 6, 'n_estimators': 400}


1500
IE: Introversion (I) - Extroversion (E) ...
Before Smote:  Counter({0: 6676, 1: 1999})
After Smote:  Counter({0: 6676, 1: 6676})
Training Score:  100.0
Testing Score:  88.01946836390864
Testing.....
* IE: Introversion (I) - Extroversion (E) Accuracy: 88.02%
* IE: Introversion (I) - Extroversion (E) Precision: 87.26%
* IE: Introversion (I) - Extroversion (E) Recall: 88.71%
* IE: Introversion (I) - Extroversion (E) F1 Score: 87.98%
NS: Intuition (N) – Sensing (S) ...
Before Smote:  Counter({0: 7478, 1: 1197})
After Smote:  Counter({0: 7478, 1: 7478})
Training Score:  100.0
Testing Score:  96.05614973262033
Testing.....
* NS: Intuition (N) – Sensing (S) Accuracy: 96.06%
* NS: Intuition (N) – Sensing (S) Precision: 96.26%
* NS: Intuition (N) – Sensing (S) Recall: 96.13%
* NS: Intuition (N) – Sensing (S) F1 Score: 96.20%
FT: Feeling (F) - Thinking (T) ...
Before Smote:  Counter({0: 4694, 1: 3981})
After Smote:  Counter({0: 4694, 1: 4694})
Training Score:  100.0
Testing Score:  78.38125665601704
Testing.....
* FT: Feeling (F) - Thinking (T) Accuracy: 78.38%
* FT: Feeling (F) - Thinking (T) Precision: 75.67%
* FT: Feeling (F) - Thinking (T) Recall: 81.19%
* FT: Feeling (F) - Thinking (T) F1 Score: 78.34%
JP: Judging (J) – Perceiving (P) ...
Before Smote:  Counter({1: 5241, 0: 3434})
After Smote:  Counter({0: 5241, 1: 5241})
Training Score:  100.0
Testing Score:  72.5798760133524
Testing.....
* JP: Judging (J) – Perceiving (P) Accuracy: 72.58%
* JP: Judging (J) – Perceiving (P) Precision: 72.72%
* JP: Judging (J) – Perceiving (P) Recall: 73.47%
* JP: Judging (J) – Perceiving (P) F1 Score: 73.09%
