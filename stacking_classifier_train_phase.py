from NER import recognizeEntity
from deduplication import deDuplication
from restructure import restructureDocument
import pickle
import json
import asyncio
from time import time
from sklearn.preprocessing import scale
from nltk.corpus import stopwords
from matplotlib import pyplot
from sklearn.metrics import f1_score
from sklearn.metrics import recall_score
from sklearn.metrics import precision_score
from sklearn.metrics import accuracy_score
from sklearn.ensemble import StackingClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split, StratifiedKFold, GridSearchCV
from catboost import CatBoostClassifier, Pool
from xgboost import XGBClassifier, plot_importance
from collections import namedtuple, Counter
from numpy import loadtxt
from imblearn.under_sampling import ClusterCentroids
from imblearn.over_sampling import SMOTE
import multiprocessing
from gensim.models.callbacks import CallbackAny2Vec
from gensim.models import Word2Vec
import gensim
from sklearn.pipeline import Pipeline
from sklearn.manifold import TSNE
from sklearn.feature_selection import SelectFromModel
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from nltk import word_tokenize
from nltk.stem import PorterStemmer, WordNetLemmatizer
import nltk
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np
import re
import logging
logging.basicConfig(filename='personality-analysis.log', level=logging.DEBUG)
# plotting
plt.style.use('ggplot')

t = time()

# Original Dataset
# data = pd.read_json('./data/clean-data/clean-data(original).json')
# mbti_dataset = pd.read_json('./data/training-data/mbti-training-data.json')

dataset_url = './data/training-data/mbti-training-data.json'
with open(dataset_url, encoding="utf8") as f:
    mbti_dataset = json.loads(f.read())
# print(data.head(16))

test_dataset_url = './data/test-data/test-data.json'
with open(test_dataset_url, encoding="utf") as f:
    test_dataset = json.loads(f.read())

# Converted Personality type lable to Binarized Format
binary_Personality = {'I': 0, 'E': 1, 'N': 0,
                      'S': 1, 'F': 0, 'T': 1, 'J': 0, 'P': 1}
binary_Personality_list = [
    {0: 'I', 1: 'E'},
    {0: 'N', 1: 'S'},
    {0: 'F', 1: 'T'},
    {0: 'J', 1: 'P'}]


def translate_personality(personality):
    # transform mbti label to binary vector
    return [binary_Personality[l] for l in personality]


def translate_back(personality):
    # transform binary vector to mbti personality label
    s = ""
    for i, l in enumerate(personality):
        s += binary_Personality_list[i][l]
    return s

# We want to remove these from the psosts
unique_type_list = ['INFJ', 'ENTP', 'INTP', 'INTJ', 'ENTJ', 'ENFJ', 'INFP', 'ENFP',
                    'ISFP', 'ISTP', 'ISFJ', 'ISTJ', 'ESTP', 'ESFP', 'ESTJ', 'ESFJ']

unique_type_list = [x.lower() for x in unique_type_list]


# Lemmatize
stemmer = PorterStemmer()
lemmatiser = WordNetLemmatizer()

# Cache the stop words for speed
cachedStopWords = stopwords.words("english")


def pre_process_data(data, train, restructure, data_deduplication, NER, remove_stop_words, remove_mbti_profiles):

    print("Preprocessing Started...")

    pre_processed_dataset = []
    personality_label_list = []
    personality_posts_list = []
    dataset_length = len(data)
    print(dataset_length)

    if restructure:
        pre_processed_dataset = restructureDocument(data, train)

    if data_deduplication and train == False:
        pre_processed_dataset = deDuplication(pre_processed_dataset)

    if NER:
        pre_processed_dataset = recognizeEntity(pre_processed_dataset)

    for row in range(len(pre_processed_dataset)):
        # Remove and clean comments
        posts = pre_processed_dataset[row]['posts']
        temp = " ".join(posts)
        temp = temp.strip()
        if remove_stop_words:
            temp = " ".join([lemmatiser.lemmatize(w)
                             for w in temp.split(' ') if w not in cachedStopWords])
        else:
            temp = " ".join([lemmatiser.lemmatize(w) for w in temp.split(' ')])

        if remove_mbti_profiles:
            for t in unique_type_list:
                temp = temp.replace(t, "")

        type_labelized = translate_personality(
            pre_processed_dataset[row]['type'])
        personality_label_list.append(type_labelized)
        personality_posts_list.append(temp)
        # personality_posts_list.append(" ".join(row[1].posts))

    personality_posts_list = np.array(personality_posts_list)
    personality_label_list = np.array(personality_label_list)

    print("Preprocessing Done...")

    return personality_posts_list, personality_label_list


personality_posts_list, personality_label_list = pre_process_data(mbti_dataset, train=True, restructure=True, data_deduplication=False, NER=True, remove_stop_words=True, remove_mbti_profiles=True)

user_posts, dummy_label = pre_process_data(test_dataset, train=False, restructure=True, data_deduplication=True, NER=True, remove_stop_words=True, remove_mbti_profiles=False)

# print(len(personality_posts_list))
# print(personality_posts_list[0])

# Only TFIDF - Feature Extraction
# Posts to a matrix of token counts
# cntizer = CountVectorizer(analyzer="word",
#                              max_features=1500,
#                              tokenizer=None,
#                              preprocessor=None,
#                              stop_words=None,
#                              lowercase=False,
#                              max_df=0.7,
#                              min_df=0.1)

# Learn the vocabulary dictionary and return term-document matrix
# X_cnt = cntizer.fit_transform(personality_posts_list)
# print(X_cnt)

# Transform the count matrix to a normalized tf or tf-idf representation
# tfizer = TfidfTransformer()

# Learn the idf vector (fit) and transform a count matrix to a tf-idf representation
# X_tfidf =  tfizer.fit_transform(X_cnt).toarray()

# print(len(X_tfidf))
# feature_names = list(enumerate(cntizer.get_feature_names()))
# print(len(feature_names))

print("Applying Tokenization...")
# TF-IDF + Word2Vec


def w2v_tokenize_text(text):
    tokens = []
    for sent in nltk.sent_tokenize(text, language='english'):
        for word in nltk.word_tokenize(sent, language='english'):
            if len(word) < 2:
                continue
            tokens.append(word)

    return tokens


print("Tokenization Done...")

personality_posts_list = list(personality_posts_list)

tokenized_post = [w2v_tokenize_text(i) for i in personality_posts_list]
# print(len(tokenized_post))

user_posts = list(user_posts)

tokenized_user_post = [w2v_tokenize_text(i) for i in user_posts]

# WORD2VEC()
class EpochLogger(CallbackAny2Vec):
    '''Callback to log information about training'''

    def __init__(self):
        self.epoch = 0

    def on_epoch_begin(self, model):
        print("Epoch #{} start".format(self.epoch))

    def on_epoch_end(self, model):
        print("Epoch #{} end".format(self.epoch))
        self.epoch += 1


epoch_logger = EpochLogger()

# Word2Vec Model Training
# cores = multiprocessing.cpu_count() # Count the number of cores in a computer, important for a parameter of the model
# w2v_model = Word2Vec(min_count=20,
#                      window=2,
#                      size=1500,
#                      sample=6e-5,
#                      alpha=0.03,
#                      min_alpha=0.0007,
#                      negative=20,
#                      workers=cores-1,
#                      callbacks=[epoch_logger])

# #BUILD_VOCAB()
# t = time()
# w2v_model.build_vocab(tokenized_post, progress_per=1000)
# print('Time to build vocab: {} mins'.format(round((time() - t) / 60, 2)))

# #TRAIN()
# w2v_model.train(tokenized_post, total_examples=w2v_model.corpus_count, epochs=100, report_delay=1)
# print('Time to train the model: {} mins'.format(round((time() - t) / 60, 2)))

# w2v_model.save('w2v_model_1500')


# Word2Vec Trained Model
w2v_model = gensim.models.Word2Vec.load('word2vec_model/w2v_model_1500')

print("Applying TF-IDF Vectorization...")

vectorizer = TfidfVectorizer(analyzer="word",
                             max_features=1500,
                             tokenizer=None,
                             preprocessor=None,
                             stop_words=None,
                             lowercase=True)
# Training Matrix
matrix = vectorizer.fit_transform([x for x in personality_posts_list])

tfidf = dict(zip(vectorizer.get_feature_names(), vectorizer.idf_))
print('vocab size: ', len(tfidf))
print("TF-IDF Vectorization Done...")

print("Building Word Vector: TF-IDF * Word2vec Model")
def buildWordVector(tokens, size):

    vec = np.zeros(size).reshape((1, size))
    count = 0.
    for word in tokens:
        try:
            vec += w2v_model[word].reshape((1, size)) * tfidf[word]
            count += 1.
        except KeyError:  # handling the case where the token is not
            # in the corpus. useful for testing.
            continue
    if count != 0:
        vec /= count

    return vec



user_matrix = vectorizer.fit_transform([x for x in user_posts])
user_tfidf = dict(zip(vectorizer.get_feature_names(), vectorizer.idf_))
print('vocab size of user post: ', len(user_tfidf))

def buildUserWordVector(tokens, size):
    
    vec = np.zeros(size).reshape((1, size))
    count = 0.
    for word in tokens:
        try:
            vec += w2v_model[word].reshape((1, size)) * user_tfidf[word]
            count += 1.
        except KeyError:  # handling the case where the token is not
            # in the corpus. useful for testing.
            continue
    if count != 0:
        vec /= count

    return vec


print("Building Word Vector Done...")

# Training Word Vector
train_vecs_w2v = np.concatenate(
    [buildWordVector(z, 1500) for z in map(lambda x: x, tokenized_post)])
train_vecs_w2v = scale(train_vecs_w2v)
# print(train_vecs_w2v.shape)


#Testing Word Vector    
test_vecs_w2v = np.concatenate(
    [buildUserWordVector(z, 1500) for z in map(lambda x: x, tokenized_user_post)])
test_vecs_w2v = scale(test_vecs_w2v)



type_indicators = ["IE: Introversion (I) - Extroversion (E)", "NS: Intuition (N) – Sensing (S)",
                   "FT: Feeling (F) - Thinking (T)", "JP: Judging (J) – Perceiving (P)"]

mbti_dimensions = ["I-E", "N-S", "F-T", "J-P"]

# Posts in tf-idf representation
# X = X_tfidf

# Posts in  tf-idf + Word2Vec representation
X = train_vecs_w2v

USER_X = test_vecs_w2v
# params = {
#     'learning_rate': [0.01, 0.1],
#     'n_estimators': [100, 200, 400],
#     'max_depth': [4, 5, 6]
# }

level0 = [
    ('knn', KNeighborsClassifier()),
    ('cart', DecisionTreeClassifier(criterion='gini',
                                    max_depth=6, max_features='auto', random_state=42)),
    ('svm', SVC(C=2, probability=True)),
    ('bayes', GaussianNB())
]

# define meta learner model
level1 = XGBClassifier(
    # n_estimators=400,
    # learning_rate=0.1,
    # max_depth=6,
    objective='binary:logistic'
)

output = []
user_output = []
# Let's train type indicator individually -- XGBoost
for l in range(len(type_indicators)):
    print("%s ..." % (type_indicators[l]))

    # Let's train type indicator individually
    Y = personality_label_list[:, l]

    # Resample Dataset - Over sampling of Minorities
    sm = SMOTE()
    X_sm, Y_sm = sm.fit_sample(X, Y)

    # split data into train and test sets
    seed = 42
    test_size = 0.2
    X_train, X_test, y_train, y_test = train_test_split(
        X_sm, Y_sm, test_size=test_size, random_state=seed)

    # fit model on training data
    eval_set = [(X_train, y_train), (X_test, y_test)]

    # skf = StratifiedKFold(n_splits=5, shuffle=True)

    # grid_cv = GridSearchCV(model, param_grid=params, n_jobs=-1, cv=skf.split(X_train, y_train), refit='accuracy_score', verbose=3)
    # define the stacking ensemble
    model = StackingClassifier(estimators=level0, final_estimator=level1)
    model.fit(X_train, y_train)

    with open('models_test/PA_model_' + mbti_dimensions[l] + '.pickle', 'wb') as f:
        pickle.dump(model, f)

    print("Training Score: ", model.score(X_train, y_train) * 100.0)
    print("Testing Score: ", model.score(X_test, y_test) * 100.0)

    # make predictions for test data
    y_pred = model.predict(X_test)
    user_pred = model.predict(USER_X)

    predictions = [round(value) for value in y_pred]

    output.append(y_pred[0])
    user_output.append(user_pred[0])

    y_pred_prob = model.predict_proba(X_test)
    print(y_pred_prob)

    user_pred_prob = model.predict_proba(USER_X)
    print(user_pred_prob)
    # evaluate predictions - testing
    print("Testing.....")
    accuracy = accuracy_score(y_test, predictions)
    precision = precision_score(y_test, predictions, zero_division=1)
    recall = recall_score(y_test, predictions)
    f1score = f1_score(y_test, predictions)
    print("* %s Accuracy: %.2f%%" % (type_indicators[l], accuracy * 100.0))
    print("* %s Precision: %.2f%%" % (type_indicators[l], precision * 100.0))
    print("* %s Recall: %.2f%%" % (type_indicators[l], recall * 100.0))
    print("* %s F1 Score: %.2f%%" % (type_indicators[l], f1score * 100.0))


print("The output is: ", translate_back(output))
print("The output is: ", translate_back(user_output))

print('Time to train the model: {} mins'.format(round((time() - t) / 60, 2)))