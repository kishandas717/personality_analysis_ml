const fs = require('fs');

let trainingDataset = JSON.parse(fs.readFileSync('./data/training-data/mbti-training-data.json'));

let mbtiDataset = [];
for (var i = 0; i < trainingDataset.length; i++) {
    var dataset = trainingDataset[i];
    let splitData = dataset.posts.split('|||');
    mbtiDataset.push({ type: dataset.type, posts: splitData})
}

// fs.writeFileSync('./data/training-data/total-posts-by-type.json', JSON.stringify(totalPostsByType, null, 2));
fs.writeFileSync('./data/training-data/spilted-mbti-training-data.json', JSON.stringify(mbtiDataset, null, 2));

