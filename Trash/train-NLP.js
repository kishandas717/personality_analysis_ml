const { NlpManager } = require('node-nlp');
const fs = require('fs');
const classifier = new NlpManager({ languages: ['en'] });

let facetData = JSON.parse(fs.readFileSync('./data/big-5-facet.json'));

console.log(facetData);

async function train() {
    for(var i = 0; i <= facetData.length; i++) {
        var facet = facetData[i];
        if(facet != undefined) {
            classifier.addDocument('en', facet.Item, facet.Facet);
            await classifier.train();
        }
    }
    await classify();
}

async function classify() {
    let cleanData = JSON.parse(fs.readFileSync('./cleanedData.json'));
    for(var i = 0; i <= 10; i++) {
        var data = cleanData[i];
        if(data != undefined && data.STATUS != "") {
            await classifier.classify(data.STATUS)
        .then((classifiedData) => {
            console.log("Intent:" + classifiedData.intent, "Value:" + classifiedData.score);
        })
        .catch((err) => {
            console.log(err);
        })
        }
    }
    
}

train();