const checkWord = require('check-word')
const words = checkWord('en');
const fs = require('fs');
const natural = require('natural');
const tokenizer = new natural.WordTokenizer();


async function train() {
    const cleanedDataset = JSON.parse(fs.readFileSync('./data/clean-data/clean-data(original).json'));
    let NLPTraingData = [];
    for (var i = 0; i <= cleanedDataset.length; i++) {
        console.log("Row ", i , "of", cleanedDataset.length)
        var dataset = cleanedDataset[i];
        var cleanPosts = [];
        if (dataset != undefined) {
            for (var j = 0; j < dataset.posts.length; j++) {
                var posts = dataset.posts[j];
                if (posts != undefined) {
                    let filteredWords = await filterValidWords(posts, dataset.type);
                    if(filteredWords != "") {
                        cleanPosts.push(filteredWords);
                    }
                }
            }
            NLPTraingData.push({type: dataset.type, posts: cleanPosts});
        }
    }
    fs.writeFileSync('./data/clean-data/cleaning-data(removed-meaningless-words).json', JSON.stringify(NLPTraingData, null, 2));
}

async function filterValidWords(data) {
    let tokenArray = tokenizer.tokenize(data);
    let filteredWords = [];
    for (var i = 0; i <= tokenArray.length; i++) {
        var x = tokenArray[i];
        if (x != undefined) {
            let check = await words.check(x);
            if (check) {
                filteredWords.push(x);
            }
        }
    }
    const uniqueSet = new Set(filteredWords);
    filteredWords = [...uniqueSet];
    return filteredWords.join(" ");
}

train();