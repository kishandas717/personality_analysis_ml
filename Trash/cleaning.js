const { NlpManager } = require('node-nlp');
const fs = require('fs');
const englishWords = require('an-array-of-english-words');
const lemmatizer = require('lemmatizer');
const winklemmatizer = require('wink-lemmatizer');
const stopWord = require('stopword');
let mbtiTypes = ["INFJ", "ENTP", "INTP", "INTJ", "ENTJ", "ENFJ", "INFP", "ENFP", "ISFP", "ISTP", "ISFJ", "ISTJ", "ESTP", "ESFP", "ESTJ", "ESFJ"];
const natural = require('natural');
const tokenizer = new natural.WordTokenizer();
const spellcheck = new natural.Spellcheck(englishWords);
const stringSimilarity = require('string-similarity');
const manager = new NlpManager({ languages: ['en'] });
const nerManager = manager.container.get('ner');

const emailRegex = /\b(\w[-._\w]*\w@\w[-._\w]*\w\.\w{2,3})\b/gi;
const urlRegex = /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/gm;
const specialCharRegex = /[~;+=!-@#$%^&*(),?"{}|<>_]/g;
const questionMarkRegex = /[�]/ig;
const youtubeUrl = /^(http(s)?:\/\/)?((w){3}.)?youtu(be|.be)?(\.com)?\/.+/gm;

manager.addRegexEntity('email', 'en', emailRegex);
manager.addRegexEntity('url', 'en', urlRegex);
// manager.addRegexEntity('special-char', 'en', specialCharRegex);
manager.addRegexEntity('question-mark', 'en', questionMarkRegex);
manager.addRegexEntity('youtube-url', 'en', youtubeUrl);

manager.train();

async function cleanData() {
  let resultData;
  let mbtiTrainingDataset = JSON.parse(fs.readFileSync('./data/training-data/spilted-mbti-training-data.json'));
  let mbtiCleanDataset = [];
  for (var i = 0; i <= mbtiTrainingDataset.length; i++) {
    console.log("Row: ", i, "of ", mbtiTrainingDataset.length)
    let userData = mbtiTrainingDataset[i];
    let cleanPosts = [];
    if (userData != undefined) {
      for (var j = 0; j <= userData.posts.length; j++) {
        let userPosts = userData.posts[j];
        if (userPosts != undefined) {
          if (typeof userPosts === "string") {
            let entityList;
            // console.log('garbbageData: ' + userPosts)
            await manager.process('en', userPosts, {})
              .then(async result => {
                entityList = result.entities;
                resultData = await removeGarbbage(entityList, userPosts);
                if (resultData.data != "") {
                  if(resultData.data.split(' ').length > 5) {
                    cleanPosts.push(resultData.data.trim());
                  }
                }
              }).catch(error => {
                console.log(error)
              });
          }
        }
      }
      mbtiCleanDataset.push({ type: userData.type, posts: cleanPosts });
    }
  }

  let garbbageData = {
    garbbageData: resultData.garbbageData
  }

  fs.writeFileSync('./data/clean-data/clean-data(original).json', JSON.stringify(mbtiCleanDataset, null, 2));
  fs.writeFileSync('./data/garbbage-data/garbbage-data(original).json', JSON.stringify(garbbageData, null, 2));
}

cleanData();
let garbbageData = [];
async function removeGarbbage(entityList, data) {
  if (entityList.length != 0) {
    for (let j = 0; j <= entityList.length; j++) {
      let entityData = entityList[j];
      if (entityData != undefined) {
        if (entityData.entity == 'email') {
          data = data.replace(entityData.sourceText, '');
          garbbageData.push(entityData.sourceText);
        }
        if (entityData.entity == 'url') {
          data = data.replace(entityData.sourceText, '');
          garbbageData.push(entityData.sourceText);
        }
        if (entityData.entity == 'special-char') {
          data = data.replace(entityData.sourceText, '');
          garbbageData.push(entityData.sourceText);
        }
        if (entityData.entity == 'number') {
          data = data.replace(entityData.sourceText, '');
          garbbageData.push(entityData.sourceText);
        }
        if (entityData.entity == 'date') {
          data = data.replace(entityData.sourceText, '');
          garbbageData.push(entityData.sourceText);
        }
        if (entityData.entity == 'daterange') {
          data = data.replace(entityData.sourceText, '');
          garbbageData.push(entityData.sourceText);
        }
        if (entityData.entity == 'datetimerange') {
          data = data.replace(entityData.sourceText, '');
          garbbageData.push(entityData.sourceText);
        }
        if (entityData.entity == 'time') {
          data = data.replace(entityData.sourceText, '');
          garbbageData.push(entityData.sourceText);
        }
        if (entityData.entity == 'duration') {
          data = data.replace(entityData.sourceText, '');
          garbbageData.push(entityData.sourceText);
        }
        if (entityData.entity == 'hashtag') {
          data = data.replace(entityData.sourceText, '');
          garbbageData.push(entityData.sourceText);
        }
        if (entityData.entity == 'phonenumber') {
          data = data.replace(entityData.sourceText, '');
          garbbageData.push(entityData.sourceText);
        }
        if (entityData.entity == 'repeated-char') {
          data = data.replace(entityData.sourceText, '');
          garbbageData.push(entityData.sourceText);
        }
        if (entityData.entity == 'question-mark') {
          data = data.replace(entityData.sourceText, '');
          garbbageData.push(entityData.sourceText);
        }
        if (entityData.entity == 'youtube-url') {
          data = data.replace(entityData.sourceText, '');
          garbbageData.push(entityData.sourceText);
        }
      }
    }
  }
  let lemmatizedData = await nounLemmatization(data);
  return {
    data: lemmatizedData,
    garbbageData: garbbageData
  };
}

async function nounLemmatization(data) {
  let nounlemmatizedString = '';
  let tokenArray = tokenizer.tokenize(data);
  for (var i = 0; i <= tokenArray.length; i++) {
    var x = tokenArray[i];
    if (x != undefined && typeof x === "string") {
      nounlemmatizedString = nounlemmatizedString + winklemmatizer.noun(x) + " ";
    }
  }
  let verbLemmatize = await verbLemmatization(nounlemmatizedString);
  return verbLemmatize;
}

async function verbLemmatization(data) {
  let verblemmatizedString = '';
  let tokenArray = tokenizer.tokenize(data);
  for (var i = 0; i <= tokenArray.length; i++) {
    var x = tokenArray[i];
    if (x != undefined && typeof x === "string") {
      verblemmatizedString = verblemmatizedString + winklemmatizer.verb(x) + " ";
    }
  }
  let adjectiveLemmatize = await adjectiveLemmatization(verblemmatizedString);
  return adjectiveLemmatize;
}

async function adjectiveLemmatization(data) {
  let adjectivelemmatizedString = '';
  let tokenArray = tokenizer.tokenize(data);

  for (var i = 0; i <= tokenArray.length; i++) {
    var x = tokenArray[i];
    if (x != undefined && typeof x === "string") {
      adjectivelemmatizedString = adjectivelemmatizedString + winklemmatizer.verb(x) + " ";
    }
  }
  let removedStopword = await removeStopWord(adjectivelemmatizedString);
  return removedStopword;
}

function removeStopWord(data) {
  let cleanData = '';
  let oldData = data.split(' ');
  let newData = stopWord.removeStopwords(oldData);
  cleanData = newData.join(' ');
  // console.log('cleanData: ' + newData.join(' '));
  return cleanData;
}


