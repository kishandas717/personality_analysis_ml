const sjs = require('simhash-js');
const fs = require('fs'); 

async function similarityHashing() {
    const cleanedDataset = JSON.parse(fs.readFileSync('./data/clean-data/cleaning-data(removed-meaningless-words).json'));
    let similarity = [];
    var simhash = new sjs.SimHash();
    for(var i = 0; i < 1; i++) {
        var dataset = cleanedDataset[i];
        console.log("Before Length: ",dataset.posts.length)
        if(dataset != undefined) {
            let posts = [];
            for (var j = 0; j < 1000; j++) {
                var x = dataset.posts[j];
                console.log("x: ", x)
                let s;
                for(var k = 0; k < 1000 - 1; k++) {
                    var y = dataset.posts[k];
                    console.log("y: ", y)
                    var x1 = simhash.hash(x);
                    var y1 = simhash.hash(y);
                    s = await sjs.Comparator.similarity(x1, y1); 
                    console.log(s)
                }
                if(s > 0 || s <= 0.6) {
                    posts.push(x);
                }
            }
            similarity.push({ type: dataset.type, posts: posts})
        }
        console.log("After Length: ",similarity[0].posts.length)
    }
    fs.writeFileSync('./data/clean-data/similarity.json', JSON.stringify(similarity, null, 2));
}

similarityHashing();