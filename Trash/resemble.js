const fs = require('fs');

let trainingDataset = JSON.parse(fs.readFileSync('./data/training-data/mbti-training-data.json'));

let mbtiTypes = {};
for (var i = 0; i < trainingDataset.length; i++) {
    var typeName = trainingDataset[i].type;
    if (!mbtiTypes[typeName]) {
        mbtiTypes[typeName] = [];
    }
    mbtiTypes[typeName].push(trainingDataset[i].posts);
}
let mbtiDataset = [];
let totalPostsByType = [];
for (var typeName in mbtiTypes) {
    let joinPosts = mbtiTypes[typeName].join();
    let splitPosts = joinPosts.split('|||');
    totalPostsByType.push({ type: typeName, totalPosts: splitPosts.length})
    mbtiDataset.push({ type: typeName, posts: splitPosts });
}

// fs.writeFileSync('./data/training-data/total-posts-by-type.json', JSON.stringify(totalPostsByType, null, 2));
fs.writeFileSync('./data/training-data/resemble-mbti-training-data.json', JSON.stringify(mbtiDataset, null, 2));

