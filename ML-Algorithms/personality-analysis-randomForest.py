import pandas as pd
import numpy as np
import re
import logging
logging.basicConfig(filename='personality-analysis.log',level=logging.DEBUG)

# plotting
import seaborn as sns
import matplotlib.pyplot as plt

import nltk
from nltk.stem import PorterStemmer, WordNetLemmatizer
from nltk.corpus import stopwords 
from nltk import word_tokenize

from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.manifold import TSNE
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import scale 

import gensim
from gensim.models import Word2Vec
from gensim.models.callbacks import CallbackAny2Vec
from time import time 
import multiprocessing

from imblearn.over_sampling import SMOTE
from imblearn.under_sampling import ClusterCentroids

from numpy import loadtxt
from xgboost import XGBClassifier
from catboost import CatBoostClassifier, Pool
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.model_selection import RandomizedSearchCV, GridSearchCV
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score

# read data
data = pd.read_json('./data/training-data/mbti-training-data.json')

# print(data.head())

b_Pers = {'I':0, 'E':1, 'N':0, 'S':1, 'F':0, 'T':1, 'J':0, 'P':1}
b_Pers_list = [{0:'I', 1:'E'}, {0:'N', 1:'S'}, {0:'F', 1:'T'}, {0:'J', 1:'P'}]

def translate_personality(personality):
    # transform mbti to binary vector
    
    return [b_Pers[l] for l in personality]

def translate_back(personality):
    # transform binary vector to mbti personality
    
    s = ""
    for i, l in enumerate(personality):
        s += b_Pers_list[i][l]
    return s

# We want to remove these from the psosts
unique_type_list = ['INFJ', 'ENTP', 'INTP', 'INTJ', 'ENTJ', 'ENFJ', 'INFP', 'ENFP',
       'ISFP', 'ISTP', 'ISFJ', 'ISTJ', 'ESTP', 'ESFP', 'ESTJ', 'ESFJ']
  
unique_type_list = [x.lower() for x in unique_type_list]


# Lemmatize
stemmer = PorterStemmer()
lemmatiser = WordNetLemmatizer()

# Cache the stop words for speed 
cachedStopWords = stopwords.words("english")

def pre_process_data(data, remove_stop_words=True, remove_mbti_profiles=True):
    
    list_personality = []
    list_posts = []
    len_data = len(data)
    i=0
    
    for row in data.iterrows():

        i+=1
        if (i % 500 == 0 or i == 1 or i == len_data):
            print("%s of %s rows" % (i, len_data))

        ##### Remove and clean comments
        posts = row[1].posts
        temp = re.sub('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', ' ', posts)
        temp = re.sub("[^a-zA-Z]", " ", temp)
        temp = re.sub(' +', ' ', temp).lower()
        if remove_stop_words:
            temp = " ".join([lemmatiser.lemmatize(w) for w in temp.split(' ') if w not in cachedStopWords])
        else:
            temp = " ".join([lemmatiser.lemmatize(w) for w in temp.split(' ')])
            
        if remove_mbti_profiles:
            for t in unique_type_list:
                temp = temp.replace(t,"")
        
        type_labelized = translate_personality(row[1].type)
        list_personality.append(type_labelized)
        list_posts.append(temp)

    list_posts = np.array(list_posts)
    list_personality = np.array(list_personality)
    return list_posts, list_personality

list_posts, list_personality  = pre_process_data(data, remove_stop_words=True)

# print(list_posts[0])

# Posts to a matrix of token counts
# cntizer = CountVectorizer(analyzer="word", 
#                              max_features=1500, 
#                              tokenizer=None,    
#                              preprocessor=None, 
#                              stop_words=None, 
#                              lowercase=False, 
#                              max_df=0.7,
#                              min_df=0.1) 

# Learn the vocabulary dictionary and return term-document matrix
# X_cnt = cntizer.fit_transform(list_posts)
# print(X_cnt)

# Transform the count matrix to a normalized tf or tf-idf representation
# tfizer = TfidfTransformer()

# Learn the idf vector (fit) and transform a count matrix to a tf-idf representation
# X_tfidf =  tfizer.fit_transform(X_cnt).toarray()
# print(X_tfidf)
# feature_names = list(enumerate(cntizer.get_feature_names()))

def w2v_tokenize_text(text):
    tokens = []
    for sent in nltk.sent_tokenize(text, language='english'):
        for word in nltk.word_tokenize(sent, language='english'):
            if len(word) < 2:
                continue
            tokens.append(word)
    return tokens

list_posts = list(list_posts)

tokenized_post = [ w2v_tokenize_text(i)  for i in list_posts ]
print(len(tokenized_post))

class EpochLogger(CallbackAny2Vec):
    '''Callback to log information about training'''
    
    def __init__(self):
        self.epoch = 0

    def on_epoch_begin(self, model):
        print("Epoch #{} start".format(self.epoch))

    def on_epoch_end(self, model):
        print("Epoch #{} end".format(self.epoch))
        self.epoch += 1

epoch_logger = EpochLogger()

w2v_model = gensim.models.Word2Vec.load('./w2v_model')

vectorizer = TfidfVectorizer(analyzer="word", 
                             max_features=1500, 
                             tokenizer=None,    
                             preprocessor=None, 
                             stop_words=None, 
                             lowercase=False, 
                             max_df=0.7,
                             min_df=0.1)
matrix = vectorizer.fit_transform([x for x in list_posts])
tfidf = dict(zip(vectorizer.get_feature_names(), vectorizer.idf_))
print('vocab size: ', len(tfidf))

def buildWordVector(tokens, size):
    vec = np.zeros(size).reshape((1, size))
    count = 0.
    for word in tokens:
        try:
            vec += w2v_model[word].reshape((1, size)) * tfidf[word]
            count += 1.
        except KeyError: # handling the case where the token is not
                         # in the corpus. useful for testing.
            continue
    if count != 0:
        vec /= count
    return vec

train_vecs_w2v = np.concatenate([buildWordVector(z, 4000) for z in map(lambda x: x, tokenized_post)])
train_vecs_w2v = scale(train_vecs_w2v)

print(train_vecs_w2v.shape)

type_indicators = [ "IE: Introversion (I) - Extroversion (E)", "NS: Intuition (N) – Sensing (S)", 
                   "FT: Feeling (F) - Thinking (T)", "JP: Judging (J) – Perceiving (P)"  ]

# Posts in tf-idf representation
# X = X_tfidf
X = train_vecs_w2v

param_grid = { 
    'n_estimators': [200, 400, 600, 800, 1000],
    'max_features': ['auto'],
    'max_depth' : [4, 6, 8, 10, 12, 14, 16],
    'criterion' :['gini', 'entropy']
}

# Let's train type indicator individually -- CatBoost
for l in range(len(type_indicators)):
    print("%s ..." % (type_indicators[l]))
    
    # Let's train type indicator individually
    Y = list_personality[:,l]

    sm = SMOTE(random_state=42)
    X_sm, Y_sm = sm.fit_sample(X, Y)

    # split data into train and test sets
    seed = 7
    test_size = 0.33
    X_train, X_test, y_train, y_test = train_test_split(X_sm, Y_sm, test_size=test_size, random_state=seed)

    # fit model on training data

    model = RandomForestClassifier(
                                n_estimators=2000,
                                criterion='gini',
                                max_features = 'auto',
                                max_depth=18,
                                random_state =42,
                                class_weight='balanced'
                                )
    # CV_rfc = GridSearchCV(estimator=model, param_grid=param_grid, cv= 5, verbose=3)

    model.fit(X_train, y_train)

    # print(CV_rfc.best_params_)

    print("Training Score: ", model.score(X_train, y_train) * 100.0)
    print("Testing Score: ", model.score(X_test, y_test) * 100.0)

    # make predictions for test data
    y_pred = model.predict(X_test)
    predictions = [value for value in y_pred]

    # evaluate predictions - testing
    print("Testing.....")
    accuracy = accuracy_score(y_test, predictions)
    precision = precision_score(y_test, predictions)
    recall = recall_score(y_test, predictions)
    f1score = f1_score(y_test, predictions)
    print("* %s Accuracy: %.2f%%" % (type_indicators[l], accuracy * 100.0))
    print("* %s Precision: %.2f%%" % (type_indicators[l], precision * 100.0))
    print("* %s Recall: %.2f%%" % (type_indicators[l], recall * 100.0))
    print("* %s F1 Score: %.2f%%" % (type_indicators[l], f1score * 100.0))