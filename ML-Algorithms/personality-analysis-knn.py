from time import time
from sklearn.preprocessing import scale
from nltk.corpus import stopwords
from matplotlib import pyplot
from sklearn.metrics import f1_score
from sklearn.metrics import recall_score
from sklearn.metrics import precision_score
from sklearn.metrics import accuracy_score
from sklearn.ensemble import StackingClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split, StratifiedKFold, GridSearchCV
from catboost import CatBoostClassifier, Pool
from xgboost import XGBClassifier, plot_importance
from collections import namedtuple, Counter
from numpy import loadtxt
from imblearn.under_sampling import ClusterCentroids
from imblearn.over_sampling import SMOTE
import multiprocessing
from gensim.models.callbacks import CallbackAny2Vec
from gensim.models import Word2Vec
import gensim
from sklearn.pipeline import Pipeline
from sklearn.manifold import TSNE
from sklearn.feature_selection import SelectFromModel
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from nltk import word_tokenize
from nltk.stem import PorterStemmer, WordNetLemmatizer
import nltk
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np
import re
import logging
logging.basicConfig(filename='personality-analysis.log', level=logging.DEBUG)

# plotting


plt.style.use('ggplot')

# Clean Data
# data = pd.read_json('./data/clean-data/clean-data(original).json')
data = pd.read_json('./data/training-data/mbti-training-data.json')
# print(data.head(16))

# Converted Personality type lable to Binarized Format
b_Pers = {'I': 0, 'E': 1, 'N': 0, 'S': 1, 'F': 0, 'T': 1, 'J': 0, 'P': 1}
b_Pers_list = [{0: 'I', 1: 'E'}, {0: 'N', 1: 'S'},
               {0: 'F', 1: 'T'}, {0: 'J', 1: 'P'}]


def translate_personality(personality):
    # transform mbti to binary vector

    return [b_Pers[l] for l in personality]


def translate_back(personality):
    # transform binary vector to mbti personality

    s = ""
    for i, l in enumerate(personality):
        s += b_Pers_list[i][l]
    return s


# We want to remove these from the psosts
unique_type_list = ['INFJ', 'ENTP', 'INTP', 'INTJ', 'ENTJ', 'ENFJ', 'INFP', 'ENFP',
                    'ISFP', 'ISTP', 'ISFJ', 'ISTJ', 'ESTP', 'ESFP', 'ESTJ', 'ESFJ']

unique_type_list = [x.lower() for x in unique_type_list]


# Lemmatize
stemmer = PorterStemmer()
lemmatiser = WordNetLemmatizer()

# Cache the stop words for speed
cachedStopWords = stopwords.words("english")


def pre_process_data(data, remove_stop_words=True, remove_mbti_profiles=True):

    list_personality = []
    list_posts = []
    len_data = len(data)
    i = 0

    for row in data.iterrows():

        i += 1
        if (i % 500 == 0 or i == 1 or i == len_data):
            print("%s of %s rows" % (i, len_data))

        # ##### Remove and clean comments
        posts = row[1].posts
        temp = re.sub(
            'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', ' ', posts)
        temp = re.sub("[^a-zA-Z]", " ", temp)
        temp = re.sub(' +', ' ', temp).lower()
        if remove_stop_words:
            temp = " ".join([lemmatiser.lemmatize(w)
                             for w in temp.split(' ') if w not in cachedStopWords])
        else:
            temp = " ".join([lemmatiser.lemmatize(w) for w in temp.split(' ')])

        if remove_mbti_profiles:
            for t in unique_type_list:
                temp = temp.replace(t, "")

        type_labelized = translate_personality(row[1].type)
        list_personality.append(type_labelized)
        list_posts.append(temp)
        # list_posts.append(" ".join(row[1].posts))

    list_posts = np.array(list_posts)
    list_personality = np.array(list_personality)
    return list_posts, list_personality


list_posts, list_personality = pre_process_data(data, remove_stop_words=True)

print(len(list_posts))

# Only TFIDF - Feature Extraction
# Posts to a matrix of token counts
# cntizer = CountVectorizer(analyzer="word",
#                              max_features=1500,
#                              tokenizer=None,
#                              preprocessor=None,
#                              stop_words=None,
#                              lowercase=False,
#                              max_df=0.7,
#                              min_df=0.1)

# Learn the vocabulary dictionary and return term-document matrix
# X_cnt = cntizer.fit_transform(list_posts)
# print(X_cnt)

# Transform the count matrix to a normalized tf or tf-idf representation
# tfizer = TfidfTransformer()

# Learn the idf vector (fit) and transform a count matrix to a tf-idf representation
# X_tfidf =  tfizer.fit_transform(X_cnt).toarray()

# print(len(X_tfidf))
# feature_names = list(enumerate(cntizer.get_feature_names()))
# print(len(feature_names))


# TF-IDF + Word2Vec

def w2v_tokenize_text(text):
    tokens = []
    for sent in nltk.sent_tokenize(text, language='english'):
        for word in nltk.word_tokenize(sent, language='english'):
            if len(word) < 2:
                continue
            tokens.append(word)
    return tokens


list_posts = list(list_posts)

tokenized_post = [w2v_tokenize_text(i) for i in list_posts]
print(len(tokenized_post))


# WORD2VEC()
class EpochLogger(CallbackAny2Vec):
    '''Callback to log information about training'''

    def __init__(self):
        self.epoch = 0

    def on_epoch_begin(self, model):
        print("Epoch #{} start".format(self.epoch))

    def on_epoch_end(self, model):
        print("Epoch #{} end".format(self.epoch))
        self.epoch += 1


epoch_logger = EpochLogger()

# Word2Vec Model Training
# cores = multiprocessing.cpu_count() # Count the number of cores in a computer, important for a parameter of the model
# w2v_model = Word2Vec(min_count=20,
#                      window=2,
#                      size=1500,
#                      sample=6e-5,
#                      alpha=0.03,
#                      min_alpha=0.0007,
#                      negative=20,
#                      workers=cores-1,
#                      callbacks=[epoch_logger])

# #BUILD_VOCAB()
# t = time()
# w2v_model.build_vocab(tokenized_post, progress_per=1000)
# print('Time to build vocab: {} mins'.format(round((time() - t) / 60, 2)))

# #TRAIN()
# w2v_model.train(tokenized_post, total_examples=w2v_model.corpus_count, epochs=100, report_delay=1)
# print('Time to train the model: {} mins'.format(round((time() - t) / 60, 2)))

# w2v_model.save('w2v_model_1500')


# WOrd2Vec Trained Model
w2v_model = gensim.models.Word2Vec.load('./w2v_model_1500')

vectorizer = TfidfVectorizer(analyzer="word",
                             max_features=1500,
                             tokenizer=None,
                             preprocessor=None,
                             stop_words=None,
                             lowercase=True)
matrix = vectorizer.fit_transform([x for x in list_posts])
tfidf = dict(zip(vectorizer.get_feature_names(), vectorizer.idf_))
print('vocab size: ', len(tfidf))


def buildWordVector(tokens, size):
    vec = np.zeros(size).reshape((1, size))
    count = 0.
    for word in tokens:
        try:
            vec += w2v_model[word].reshape((1, size)) * tfidf[word]
            count += 1.
        except KeyError:  # handling the case where the token is not
            # in the corpus. useful for testing.
            continue
    if count != 0:
        vec /= count
    return vec


train_vecs_w2v = np.concatenate(
    [buildWordVector(z, 1500) for z in map(lambda x: x, tokenized_post)])
train_vecs_w2v = scale(train_vecs_w2v)

print(train_vecs_w2v.shape)


type_indicators = ["IE: Introversion (I) - Extroversion (E)", "NS: Intuition (N) – Sensing (S)",
                   "FT: Feeling (F) - Thinking (T)", "JP: Judging (J) – Perceiving (P)"]

# Posts in tf-idf representation
# X = X_tfidf

# Posts in  tf-idf + Word2Vec representation
X = train_vecs_w2v

# Let's train type indicator individually -- XGBoost
for l in range(len(type_indicators)):
    print("%s ..." % (type_indicators[l]))

    # Let's train type indicator individually
    Y = list_personality[:, l]

    # Resample Dataset - Over sampling of Minorities
    sm = SMOTE()
    X_sm, Y_sm = sm.fit_sample(X, Y)

    print("Before Smote: ", Counter(Y))
    print("After Smote: ", Counter(Y_sm))

    # split data into train and test sets
    seed = 42
    test_size = 0.2
    X_train, X_test, y_train, y_test = train_test_split(
        X_sm, Y_sm, test_size=test_size, random_state=seed)

    # define the stacking ensemble
    model = KNeighborsClassifier(n_neighbors=5)

    # fit model on training data
    eval_set = [(X_train, y_train), (X_test, y_test)]

    model.fit(X_train, y_train)

    print("Training Score: ", model.score(X_train, y_train) * 100.0)
    print("Testing Score: ", model.score(X_test, y_test) * 100.0)

    # make predictions for test data
    y_pred = model.predict(X_test)
    predictions = [round(value) for value in y_pred]

    # evaluate predictions - testing
    print("Testing.....")
    accuracy = accuracy_score(y_test, predictions)
    precision = precision_score(y_test, predictions, zero_division=1)
    recall = recall_score(y_test, predictions)
    f1score = f1_score(y_test, predictions)
    print("* %s Accuracy: %.2f%%" % (type_indicators[l], accuracy * 100.0))
    print("* %s Precision: %.2f%%" % (type_indicators[l], precision * 100.0))
    print("* %s Recall: %.2f%%" % (type_indicators[l], recall * 100.0))
    print("* %s F1 Score: %.2f%%" % (type_indicators[l], f1score * 100.0))
