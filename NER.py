import spacy
from spacy.pipeline import EntityRuler
import json
import re

EMAIL_REGEX = r'\b(\w[-._\w]*\w@\w[-._\w]*\w\.\w{2,3})\b'
URL_REGEX = r'^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$'
SPECIALCHAR_REGEX = r'[~;+=!-@#$%^&*(),?"{}|<>_]'
QUESTIONMARK_REGEX = r'[�]'
YOUTUBEURL_REGEX = r'^(http(s)?:\/\/)?((w){3}.)?youtu(be|.be)?(\.com)?\/.+'

nlp = spacy.load("en_core_web_sm")
custom_ruler = EntityRuler(nlp)

patterns = [
    {"label": "EMAIL_REGEX", "pattern": [{'TEXT': {'REGEX': EMAIL_REGEX}}]},
    {"label": "URL_REGEX", "pattern": [{'TEXT': {'REGEX': URL_REGEX}}]},
    {"label": "SPECIALCHAR_REGEX", "pattern": [{'TEXT': {'REGEX': SPECIALCHAR_REGEX}}]},
    {"label": "QUESTIONMARK_REGEX", "pattern": [{'TEXT': {'REGEX': QUESTIONMARK_REGEX}}]},
    {"label": "YOUTUBEURL_REGEX", "pattern": [{'TEXT': {'REGEX': YOUTUBEURL_REGEX}}]}]

custom_ruler.add_patterns(patterns)
nlp.add_pipe(custom_ruler, before="ner")

# dataset_url = './data/training-data/spilted-mbti-training-data.json'
# with open(dataset_url, encoding="utf8") as f:
#     data = json.loads(f.read())


def preprocessEntity(doc):
    
    new_doc = doc
    doc_nlp = nlp(doc)
    doc_entities = doc_nlp.ents

    for entity in doc_entities:
        if entity != None:
            if entity.label_ == "PERSON":
                new_doc = new_doc.replace(entity.text, "")
            elif entity.label_ == "NORP":
                new_doc = new_doc.replace(entity.text, "")
            elif entity.label_ == "FAC":
                new_doc = new_doc.replace(entity.text, "")
            elif entity.label_ == "ORG":
                new_doc = new_doc.replace(entity.text, "")
            elif entity.label_ == "GEP":
                new_doc = new_doc.replace(entity.text, "")
            elif entity.label_ == "LOC":
                new_doc = new_doc.replace(entity.text, "")
            elif entity.label_ == "PRODUCT":
                new_doc = new_doc.replace(entity.text, "")
            elif entity.label_ == "EVENT":
                new_doc = new_doc.replace(entity.text, "")
            elif entity.label_ == "WORK_OF_ART":
                new_doc = new_doc.replace(entity.text, "")
            elif entity.label_ == "LAW":
                new_doc = new_doc.replace(entity.text, "")
            elif entity.label_ == "LANGUAGE":
                new_doc = new_doc.replace(entity.text, "")
            elif entity.label_ == "DATE":
                new_doc = new_doc.replace(entity.text, "")
            elif entity.label_ == "TIME":
                new_doc = new_doc.replace(entity.text, "")
            elif entity.label_ == "PERCENT":
                new_doc = new_doc.replace(entity.text, "")
            elif entity.label_ == "MONEY":
                new_doc = new_doc.replace(entity.text, "")
            elif entity.label_ == "QUANTITY":
                new_doc = new_doc.replace(entity.text, "")
            elif entity.label_ == "ORDINAL":
                new_doc = new_doc.replace(entity.text, "")
            elif entity.label_ == "CARDINAL":
                new_doc = new_doc.replace(entity.text, "")
            elif entity.label_ == "EMAIL_REGEX":
                new_doc = new_doc.replace(entity.text, "")
            elif entity.label_ == "URL_REGEX":
                new_doc = new_doc.replace(entity.text, "")
            elif entity.label_ == "SPECIALCHAR_REGEX":
                new_doc = new_doc.replace(entity.text, "")
            elif entity.label_ == "QUESTIONMARK_REGEX":
                new_doc = new_doc.replace(entity.text, "")
            elif entity.label_ == "YOUTUBEURL_REGEX":
                new_doc = new_doc.replace(entity.text, "")    
    
    new_doc = re.sub('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', ' ', new_doc)
    new_doc = re.sub(' +', ' ', new_doc).lower()
    new_doc = new_doc.strip()
    return new_doc

def recognizeEntity(dataset):

    clean_data = []

    print("")
    print("Named Entity Recognition Started...")

    for i in range(len(dataset)):
        document = dataset[i]
        if document != None:
            posts = []
            new_doc = ""
            for j in range(len(document["posts"])):
                doc = document["posts"][j]
                if doc != None:
                    new_doc = preprocessEntity(doc)
                if new_doc != "":
                    posts.append(new_doc)
        clean_data.append({"type": document["type"], "posts": posts})

    print("Named Entity Recognition Done...")

    return clean_data

# recognizeEntity(data)

# with open('./data/clean-data/sample.json', 'w') as outfile:
#             json.dump(recognizeEntity(data), outfile, indent=2)


# preprocessEntity("enfp and intj moments  https://www.youtube.com/watch?v=iz7lE1g4XM4  sportscenter not top ten plays  https://www.youtube.com/watch?v=uCdfze1etec  pranks")